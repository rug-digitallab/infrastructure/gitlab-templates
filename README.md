# GitLab CI Templates
This repository contains a collection of GitLab CI templates for use in Digital Lab projects. Currently, there are 
CI templates for:
- Gradle
- Terraform

# Prerequisites

## Gradle

The Gradle templates require the following environment variables to be set:
- `GRADLE_CLI: "./gradlew --gradle-user-home gradle-home/ --build-cache -i"`
- `JAVA_OPTS: "-Xmx4G"` Fixes spotbugs OOM error

The `unit-tests-dev-containters.yml` template requires the following environment variables to be set:
- `DOCKER_HOST: "tcp://docker:2375"`
- `DOCKER_TLS_CERTDIR: ""`
- `DOCKER_DRIVER: overlay2`

The `release.yml` template requires the following environment variables to be set:
- `GITLAB_TOKEN: $GITLAB_RELEASE_ACCESS_TOKEN`

The GitLab Access Token should be a project or group access token with the `developer` role and `api` permissions. Next, create 
the `GITLAB_RELEASE_ACCESS_TOKEN` CI/CD variable as a `masked` and `protected` variable. 

# Usage
To include the templates in your `.gitlab-ci.yml` file, add the following lines:

```yaml
include:
- project: rug-digitallab/resources/gitlab-ci-templates
  ref: develop
  file:
    - path/to/template.yml
    - path/to/another-template.yml
```

## Containers
To publish a container, you can include the `containers/publish.yml` template in your `.gitlab-ci.yml`.

This template will provide container building, security scanning, and container publishing. An example usage of this template is:
```yaml
include:
  - project: rug-digitallab/resources/gitlab-ci-templates
    ref: develop
    file: containers/publish.yml
    inputs:
      image_name: image-name-a
      project_folder: folder-b
  - project: rug-digitallab/resources/gitlab-ci-templates
    ref: develop
    file: containers/publish.yml
    inputs:
      image_name: image-name-b
      project_folder: folder-b

```

## Static analysis for gradle
For gradle, a static analysis template is available at `gradle/static-analyis/static-analysis.yml`.

This template will provide automated analysis for:
- Dependency scanning
- Secret detection
- Code quality (SonarCloud + SemGrep)
- SAST (SpotBugs)

This template can be used as follows:
```yaml
include:
  - project: rug-digitallab/resources/gitlab-ci-templates
    ref: develop
    file: gradle/static-analysis-static-analyis.yml
```

### SonarCloud
SonarCloud makes use of the unit test output for code coverage. As such, it should depend on the unit test job if present.
This can be configured using the `unit_test_stage` input. The default value is `unit-test`, but this must be changed for dev container unit tests for example. If no unit test stage is present, please set the value to `""`.
